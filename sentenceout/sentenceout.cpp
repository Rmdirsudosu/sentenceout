﻿// sentenceout.cpp : Defines the entry point for the application.
//

#include "sentenceout.h"

using namespace std;

int main()
{
	std::string sentence;
	std::cout << "Enter your sentence : ";
	std::getline(std::cin, sentence);
	std::cout << std::endl;

	for (int i = 0; i < sentence.length() + 1; i++) {
		if (sentence[i] != ' ')
			std::cout << sentence.substr(0, i) << std::endl;
	}

	std::cin.get();

	return 0;
}
